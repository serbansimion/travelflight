<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = Post::orderBy('created_at','desc')->latest()->paginate(40);
        return view('homepage', ['posts' => $posts]);
    }

    public function showPost($id)
    {
        $post = Post::find($id);
        $recentPosts = Post::latest()->take(4)->get()->except($id)->take(3);
        $postAvailable = true;
        if(!$post) {
            $postAvailable = false;
        }

        return view('post_details', ['post' => $post, 'recentPosts' => $recentPosts, 'postAvailable' => $postAvailable]);
    }
}
