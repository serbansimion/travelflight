<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at','desc')->get();
        return view('dashboard', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('add_posts');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post;
        $post->title = $request->title;
        $post->destination = $request->destination;
        $post->period = $request->period;
        $post->image_link = $request->image_link;
        $post->details_link = $request->details_link;
        if($post->save()) {
            return back()->with('success','Post created successfully!');
        }
        else {
            return back()->with('error','Error while saving the post!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        if(!$post) {
            return back()->with('error', 'Post with id '.$id.' not found in the database!');  
        }
        return view('edit_posts', ['post' => $post]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        if(!$post) {
            return back()->with('error', 'Post with id '.$id.' not found in the database!');  
        }
        $post->title = $request->title;
        $post->destination = $request->destination;
        $post->period = $request->period;
        $post->image_link = $request->image_link;
        $post->details_link = $request->details_link;
        if($post->save()) {
            return back()->with('success','Post updated successfully!');
        }
        else {
            return back()->with('error','Post not updated!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $post = Post::find($request->post_id);
        if(!$post) {
            return back()->with('error', 'Post with id '.$request->post_id.' not found in the database!');  
        }
        if($post->delete()) {
            return back()->with('success','Post deleted successfully!');
        }
        else {
            return back()->with('error','Error while deleting post!');
        }
    }
}
