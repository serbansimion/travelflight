@extends('layouts.app')
@section('content')
@include('flash-messages')
<div class="container">
    <form class="border border-light p-5" method="post" action="{{ route('store') }}">
        {{ csrf_field() }}
        <a role="button" href="{{route('dashboard')}}" class="btn btn-rounded btn-success btn-sm">All posts</a>
        <p class="h4 mb-4 text-center">New post</p>
    
        <input type="text" id="title" name="title" class="form-control mb-4" placeholder="Title" required>
        <input type="text" id="destination" name="destination" class="form-control mb-4" placeholder="Destination" required>
        <input type="text" id="period" name="period" class="form-control mb-4" placeholder="Period" required>
        <input type="text" id="image_link" name="image_link" class="form-control mb-4" placeholder="Image link" required>
        <input type="text" id="details_link" name="details_link" class="form-control mb-4" placeholder="Details link" required>

        <div class="text-center py-4 mt-3">
            <button class="btn btn-info" type="submit">Add post</button>
        </div>
    </form>
</div>

@endsection