@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard
                    <a role="button" href="{{route('add.posts')}}" class=" btn-floating btn-info btn-sm float-right">Add post</a>
                </div>
                @include('flash-messages')
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @foreach($posts as $post)
                    <div class="media border" style="margin-bottom:1%">
                        <img class="mr-3" src="{{$post->image_link}}" alt="Generic placeholder image" style="height:60px; width:60px" alt="">
                        <div class="media-body">
                            <p class="hyphenate">
                                {{$post->id}} - 
                                <b>{{$post->title}}</b>
                                <br><b>Destination:</b> {{$post->destination}}
                                <br><b>Period:</b> {{$post->period}}
                                <br><b>Link:</b> {{$post->details_link}}
                                <br><b>Created:</b> {{$post->created_at}}
                            </p>
                            <div style="margin-bottom:1%">
                                <a role="button" id="deleteBtn" name="deleteBtn" class="btn-floating btn-danger btn-sm deleteUser"
                                    data-toggle="modal" data-target="#deleteModal" data-postid="{{$post->id}}">Delete</a>
                                <a role="button" href="{{route('edit', $post->id)}}" class="btn-floating btn-info btn-sm">Edit</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<!-- Central Modal Small -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-sm" role="document">

    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Delete confirmation!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('delete')}}" method="post">
            {{method_field('delete')}}
            {{csrf_field()}}
        <div class="modal-body">
            <p class="text-center">
                Are you sure you want to delete this?
            </p>
            <input type="hidden", name="post_id" id="post_id" value="">    
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-sm">Yes</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Central Modal Small -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        //triggered when modal is about to be shown
        $('#deleteModal').on('show.bs.modal', function(e) {
            //get data-id attribute of the clicked element
            var postId = $(e.relatedTarget).data('postid');
            //populate the textbox
            $(e.currentTarget).find('input[name="post_id"]').val(postId);
        });
    });
</script>
