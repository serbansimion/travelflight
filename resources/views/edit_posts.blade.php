@extends('layouts.app')
@section('content')
@include('flash-messages')
<div class="container">
    <form class="border border-light p-5" method="post" action="{{ route('update', ['id' => $post->id]) }}">
        {{ csrf_field() }}
        <a role="button" href="{{route('dashboard')}}" class="btn btn-rounded btn-success btn-sm">All posts</a>
        <p class="h4 mb-4 text-center">Edit post</p>
    
        <input type="text" id="title" name="title" class="form-control mb-4" placeholder="Title" value="{{$post->title}}" required>
        <input type="text" id="destination" name="destination" class="form-control mb-4" placeholder="Destination" value="{{$post->destination}}" required>
        <input type="text" id="period" name="period" class="form-control mb-4" placeholder="Period" value="{{$post->period}}" required>
        <input type="text" id="image_link" name="image_link" class="form-control mb-4" placeholder="Image link" value="{{$post->image_link}}" required>
        <input type="text" id="details_link" name="details_link" class="form-control mb-4" placeholder="Details link" value="{{$post->details_link}}" required>

        <div class="text-center py-4 mt-3">
            <button class="btn btn-info" type="submit">Update post</button>
        </div>
    </form>
</div>
@endsection