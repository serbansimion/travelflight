@extends('layouts.master')

@section('pageintro')
  <!-- Full Page Intro -->
  <div class="view jarallax intropage" data-jarallax='{"speed": 0.2}'>
    <!-- Mask & flexbox options-->
    <div class="mask rgba-white-light d-flex justify-content-center align-items-center">
      <!-- Content -->
      <div class="container">
        <!--Grid row-->
        <div class="row">
          <!--Grid column-->
          <div class="col-md-12 white-text text-center">
            <h1 class="display-3 mb-0 pt-md-5 pt-5 white-text font-weight-bold wow fadeInDown" data-wow-delay="0.3s">TRAVEL
              <a class="indigo-text font-weight-bold light-blue-text">FLIGHT</a>
            </h1>
            <h1 class="pt-md-5 pt-sm-2 pt-5 pb-md-5 pb-sm-3 pb-5 white-text font-weight-bold wow fadeInDown"
              data-wow-delay="0.3s">TravelFlight helps you find the best travel flights and accomodation deals!</h1>
          </div>
          <!--Grid column-->
        </div>
        <!--Grid row-->
      </div>
      <!-- Content -->
    </div>
    <!-- Mask & flexbox options-->
  </div>
  <!-- Full Page Intro -->
@endsection

@section('content')
  <!-- Section: Blog v.1 -->
  <section class="my-5">
      <!-- Section heading -->
      <!-- Grid row -->
      @foreach ($posts as $key=>$post)
        @if($key%2 == 0)
          @include('image_left_post', array('post' => $post))
        @else
          @include('image_right_post', array('post' => $post))
        @endif
      @endforeach
      <!-- Grid row -->
      {{ $posts->links() }}
  </section>
@endsection