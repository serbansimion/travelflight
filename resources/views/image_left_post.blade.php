<div class="row">
  <!-- Grid column -->
  <div class="col-lg-5 col-xl-4">
    <!-- Featured image -->
    <div class="overlay rounded z-depth-1-half mb-lg-0 mb-4">
      <img class="img-fluid" src="{{$post->image_link}}" alt="Destination image">
      <a>
        <div class="mask rgba-white-slight"></div>
      </a>
    </div>
  </div>
  <!-- Grid column -->
  <div class="col-lg-7 col-xl-8">

    <!-- Category -->
    <a href="" class="green-text">
      <h6 class="font-weight-bold mb-3 light-blue-text"><i class="fas fa-plane pr-2"></i>Travel</h6>
    </a>
    <!-- Post title -->
    <h2 class="font-weight-bold mb-3">
      <a href="{{$post->details_link}}">
        <strong>{{$post->title}}</strong>
      </a>
    </h2>
    <!-- Excerpt -->
    <h6><b>Destination:</b> {{$post->destination}}</h6>
    <h6><b>Travel dates:</b> {{$post->period}}</h6>
    <!-- Read more button -->
    <div class="row justify-content-between align-items-center">
      <a href="{{$post->details_link}}" class="btn blue-gradient btn-md">View deal</a>
      <div class="addthis_inline_share_toolbox" 
        data-url="{{route('post.details', ['id' => $post->id])}}" data-href="https://travelflight.eu"></div>
      </div>
    </div>
  <!-- Grid column -->
</div>
<!-- Grid row -->

<hr class="my-5">