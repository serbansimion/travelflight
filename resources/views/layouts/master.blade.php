<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-134291869-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-134291869-1');
    </script>

  <meta charset="utf-8">
  <meta name="google-site-verification" content="wk7rRo6AegDMsXRHVznfX3N1HgF8ripnEvbHB1qAsBw" />
  <meta name="B-verify" content="2922df3b8dcdea1023e39050ead0090ec4591ac7" />
  <meta name="agd-partner-manual-verification" />
  <meta name="msvalidate.01" content="F2A9751BF77D1B9B55849C2089E98DEE" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="TravelFlight.eu is the best place to find super cheap travel flights deals and also best accomodation deals for desired dates.">
  <meta name="keywords" content="view deal, travel dates, deal travel, travel flights, dates february, view deal travel, deal travel flights, travel dates february, june view deal, travel dates april">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="/img/favicon/site.webmanifest">
  <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
  <title>TravelFlight - Cheap travel flights deals</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="/css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="/css/style.css" rel="stylesheet">
  @yield('addmeta')
</head>

<!--Main Navigation-->
<header>
  @include('navbar')
</header>

@yield('pageintro')

<!--Main Navigation-->
<body>
  <div class="container">
    @yield('content')
  </div>

  <!-- SCRIPTS -->
  <!-- JQuery -->
  {{-- <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
  <!-- Bootstrap tooltips -->
  {{-- <script type="text/javascript" src="{{resource_path('js/popper.min.js')}}"></script> <!-- fix import of js--> --}}
  <!-- Bootstrap core JavaScript -->
  {{-- <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.js"></script> --}}
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c600414c703e8b4"></script>
  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  <script>
    (adsbygoogle = window.adsbygoogle || []).push({
      google_ad_client: "ca-pub-7958007864261124",
      enable_page_level_ads: true
    });
  </script>
</body>
<!-- WhatsHelp.io widget -->
<script type="text/javascript">
  (function () {
      var options = {
          facebook: "2284336581634275", // Facebook page ID
          call_to_action: "", // Call to action
          button_color: "#129BF4", // Color of button
          position: "right", // Position may be 'right' or 'left'
          order: "facebook", // Order of buttons
      };
      var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
      var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
      s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
      var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
  })();
</script>
<!-- /WhatsHelp.io widget -->
@include('footer')

</html>
