<nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
  <a class="navbar-brand white-text font-roboto" href="{{ route('home') }}"><img class="logo" src="{{asset('img/svg/logo.svg')}}"><strong>  TravelFlight</strong></a>
  <ul class="navbar-nav ml-auto nav-flex-icons">
      <li class="nav-item">
          <div class="addthis_inline_share_toolbox_p0ra"></div>
      </li>
  </ul>
</nav>