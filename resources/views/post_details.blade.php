@extends('layouts.master')

@section('addmeta')
    <meta property="og:title" content="TravelFlight.eu"/>
    <meta property="og:description" content="{{$post->title}}"/>
    <meta property="og:type" content="blog post"/>
    <meta property="og:image" content="{{$post->image_link}}"/>
@endsection

@section('content')
    <hr class="my-5">
    <div class="row">
        @if($postAvailable)
            <!-- Grid column -->
            <div class="col-lg-5 col-xl-4">
                <!-- Featured image -->
                <div class="overlay rounded z-depth-1-half mb-lg-0 mb-4">
                <img class="img-fluid" src="{{$post->image_link}}" alt="Destination image">
                <a>
                    <div class="mask rgba-white-slight"></div>
                </a>
                </div>
            </div>
            <!-- Grid column -->
            <div class="col-lg-7 col-xl-8">
            
                <!-- Category -->
                <a href="" class="green-text">
                <h6 class="font-weight-bold mb-3 light-blue-text"><i class="fas fa-plane pr-2"></i>Travel</h6>
                </a>
                <!-- Post title -->
                <h2 class="font-weight-bold mb-3">
                <a href="{{$post->details_link}}">
                    <strong>{{$post->title}}</strong>
                </a>
                </h2>
                <!-- Excerpt -->
                <h6><b>Destination:</b> {{$post->destination}}</h6>
                <h6><b>Travel dates:</b> {{$post->period}}</h6>
                <!-- Read more button -->
                <div class="row justify-content-between align-items-center">
                    <a href="{{$post->details_link}}" class="btn blue-gradient btn-md">View deal</a>
                    <div class="addthis_inline_share_toolbox" 
                        data-url="{{route('post.details', ['id' => $post->id])}}" data-href="https://travelflight.eu">
                    </div>
                </div>
            </div>
            <!-- Grid column -->
            @else
            <div class="col-lg-4 col-xl-4">
                <!-- Featured image -->
                <div class="overlay rounded z-depth-1-half mb-lg-0 mb-4">
                    <img class="img-fluid" src="{{asset('img/utils/notfound.png')}}" alt="Destination image">
                </div>
            </div>
            <!-- Grid column -->
            <div class="col-lg-7 col-xl-8">
                <!-- Post title -->
                <h2 class="font-weight-bold mb-3 blue-text">
                    <strong>This offer is not available anymore, please look over the recent deals!</strong>
                </h2>
            </div>
        @endif
    </div>
    <!-- Grid row -->
        
    <hr class="my-5">

    <!-- Section: Blog v.2 -->
    <section class="text-center my-5">

        <!-- Section heading -->
        <h1 class="h1-responsive font-weight-bold my-10">Recent flights deals</h1>
        <!-- Section description -->
        <p class="dark-grey-text w-responsive mx-auto mb-5">View the most recent deals for cheap flights and accommodation added to TravelFlight.eu!</p>
      
        <!-- Grid row -->
        <div class="row">
            @foreach($recentPosts as $recentPost)
                @include('recent_post', array('post' => $recentPost))
            @endforeach
        </div>
        <!-- Grid row -->
    </section>
        <div class="row justify-content-center">
            <a href="{{route('home')}}" class="btn blue-gradient btn-md">View more deals like this!</a>  
        </div>
        <!-- Section: Blog v.2 -->
        <hr class="my-5">
@endsection
