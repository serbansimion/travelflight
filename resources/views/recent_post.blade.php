<!-- Grid column -->
<div class="col-lg-4 col-md-12 mb-lg-0 mb-4">

    <!-- Featured image -->
    <div class="overlay rounded z-depth-2 mb-4">
        <img class="img-fluid" src="{{$post->image_link}}" alt="Destination image">
        <a>
        <div class="mask rgba-white-slight"></div>
        </a>
    </div>

    <!-- Category -->
    <a href="#!" class="blue-text">
        <h6 class="font-weight-bold mb-3 light-blue-text"><i class="fas fa-map pr-2"></i>Travel</h6>
    </a>
    <!-- Post title -->
    <h4 class="font-weight-bold mb-3">
        <a href="{{$post->details_link}}">
            <strong>{{$post->title}}</strong>
        </a>
    </h4>

    <!-- Excerpt -->
    <h6><b>Destination:</b> {{$post->destination}}</h6>
    <h6><b>Travel dates:</b> {{$post->period}}</h6>
    <!-- Read more button -->
    <a href="{{$post->details_link}}" class="btn blue-gradient btn-md">View deal</a>  

</div>    