<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/details/{id}', 'HomeController@showPost')->name('post.details');
Auth::routes();

Route::get('/dashboard', 'PostController@index')->name('dashboard');
Route::get('/create', 'PostController@create')->name('add.posts');
Route::delete('/delete', 'PostController@destroy')->name('delete');
Route::get('/edit/{id}', 'PostController@edit')->name('edit');
Route::post('/edit/{id}', 'PostController@update')->name('update');
Route::post('/store', 'PostController@store')->name('store');
